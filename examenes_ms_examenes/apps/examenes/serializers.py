from django.db import transaction
from rest_framework import serializers

from .models import *


class RespuestaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Respuesta
        fields = ('id', 'respuesta', 'imagen', 'es_correcta')


class ModificarRespuestaSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Respuesta
        fields = ('id', 'respuesta', 'imagen', 'es_correcta')

    def to_internal_value(self, data):
        return super().to_internal_value(data)


class PreguntaSerializer(serializers.HyperlinkedModelSerializer):
    respuestas = RespuestaSerializer(many=True, read_only=False)

    class Meta:
        model = Pregunta
        fields = (
            'creador', 'enunciado', 'respuestas', 'id', 'url'
        )

    def create(self, validated_data):
        respuestas_data = validated_data.pop('respuestas')

        pregunta = Pregunta.objects.create(**validated_data)
        for respuesta in respuestas_data:
            Respuesta.objects.create(pregunta=pregunta, **respuesta)

        return pregunta

    def update(self, instance, validated_data):
        respuestas_data = validated_data.get('respuestas', [])
        # respuestas_nuevas = [respuesta for respuesta in respuestas_data if respuesta.get('id', None) is None]
        # existed_respuestas = [respuesta for respuesta in respuestas_data if respuesta.get('id', None) is not None]

        with transaction.atomic():
            instance.enunciado = validated_data.get('enunciado', instance.enunciado)
            instance.creador = validated_data.get('creador', instance.creador)
            instance.save()
            for respuesta in respuestas_data:
                id = respuesta.get('id', None)
                if id:
                    try:
                        respuesta_instance = Respuesta.objects.get(id=id, pregunta__id=instance.id)
                        respuesta_instance.respuesta = respuesta.get('respuesta', respuesta_instance.respuesta)
                        respuesta_instance.imagen = respuesta.get('imagen', respuesta_instance.imagen)
                        respuesta_instance.es_correcta = respuesta.get('es_correcta', respuesta_instance.es_correcta)
                        respuesta_instance.save()
                    except Respuesta.DoesNotExist:
                        raise Exception("CREANDO CON ID")
                        Respuesta.objects.create(pregunta=instance, **respuesta)
                else:
                    Respuesta.objects.create(pregunta=instance, **respuesta)
            # old_respuestas = list(instance.respuestas.all().values())

            # removed_respuestas = old_respuestas - existed_respuestas
            # for respuesta in removed_respuestas:
            #     respuesta.delete()

        return instance


class ModificarPreguntaSerializer(serializers.HyperlinkedModelSerializer):
    respuestas = ModificarRespuestaSerializer(many=True, read_only=False)

    class Meta:
        model = Pregunta
        fields = (
            'id', 'url', 'creador', 'enunciado', 'respuestas'
        )

    def create(self, validated_data):
        respuestas_data = validated_data.pop('respuestas')

        pregunta = Pregunta.objects.create(**validated_data)
        for respuesta in respuestas_data:
            Respuesta.objects.create(pregunta=pregunta, **respuesta)

        return pregunta

    def update(self, instance, validated_data):
        respuestas_data = validated_data.get('respuestas', [])
        # respuestas_nuevas = [respuesta for respuesta in respuestas_data if respuesta.get('id', None) is None]
        # existed_respuestas = [respuesta for respuesta in respuestas_data if respuesta.get('id', None) is not None]

        with transaction.atomic():
            instance.enunciado = validated_data.get('enunciado', instance.enunciado)
            instance.creador = validated_data.get('creador', instance.creador)
            instance.save()
            for respuesta in respuestas_data:
                id = respuesta.get('id', None)
                if id:
                    try:
                        respuesta_instance = Respuesta.objects.get(id=id, pregunta__id=instance.id)
                        respuesta_instance.respuesta = respuesta.get('respuesta', respuesta_instance.respuesta)
                        respuesta_instance.imagen = respuesta.get('imagen', respuesta_instance.imagen)
                        respuesta_instance.es_correcta = respuesta.get('es_correcta', respuesta_instance.es_correcta)
                        respuesta_instance.save()
                    except Respuesta.DoesNotExist:
                        Respuesta.objects.create(pregunta=instance, **respuesta)
                else:
                    Respuesta.objects.create(pregunta=instance, **respuesta)
            # old_respuestas = list(instance.respuestas.all().values())

            # removed_respuestas = old_respuestas - existed_respuestas
            # for respuesta in removed_respuestas:
            #     respuesta.delete()

        return instance


class ExamenSerializer(serializers.ModelSerializer):
    preguntas_originales = serializers.PrimaryKeyRelatedField(queryset=Pregunta.objects.all(), write_only=True, many=True)

    class Meta:
        model = Examen
        fields = (
            'id', 'titulo', 'profesor', 'grupo', 'fecha_inicio', 'fecha_fin', 'preguntas_originales'
        )

    def create(self, validated_data):
        preguntas_originales = validated_data.pop('preguntas_originales')
        examen = Examen.objects.create(**validated_data)
        examen.preguntas_originales.set(preguntas_originales)
        return examen

    def update(self, instance, validated_data):
        preguntas_originales = validated_data.pop('preguntas_originales')
        examen = super().update(instance, validated_data)
        examen.preguntas_originales.set(preguntas_originales)
        return examen

    def save(self, **kwargs):
        examen = super().save(**kwargs)
        examen.actualizar_preguntas()


class ObtenerExamenSerializer(serializers.HyperlinkedModelSerializer):
    preguntas_originales = PreguntaSerializer(many=True)
    preguntas = PreguntaSerializer(many=True)

    class Meta:
        model = Examen
        depth = 1
        fields = (
            'id', 'url', 'titulo', 'profesor', 'grupo', 'fecha_inicio', 'fecha_fin', 'iniciado', 'terminado', 'preguntas_originales', 'preguntas'
        )


class RespuestaExamenResueltoSerializer(serializers.ModelSerializer):
    class Meta:
        model = RespuestaExamenResuelto
        fields = ('id', 'respuesta', 'imagen', 'es_correcta')


class PreguntaExamenResueltoSerializer(serializers.ModelSerializer):
    respuestas_seleccionadas = serializers.PrimaryKeyRelatedField(queryset=RespuestaExamenResuelto.objects.all(), write_only=True, many=True)

    class Meta:
        model = PreguntaExamenResuelto
        fields = (
            'id', 'respuestas_seleccionadas'
        )

    def update(self, instance, validated_data):
        respuestas = validated_data.pop('respuestas_seleccionadas')
        pregunta = super().update(instance, validated_data)
        pregunta.respuestas_seleccionadas.set(respuestas)
        return pregunta


class ObtenerPreguntaExamenResueltoSerializer(serializers.ModelSerializer):
    respuestas = RespuestaExamenResueltoSerializer(many=True, read_only=True)
    respuestas_seleccionadas = RespuestaExamenResueltoSerializer(many=True, read_only=True)
    resultado = serializers.SerializerMethodField()
    calificacion = serializers.SerializerMethodField()

    def get_resultado(self, instance):
        return instance.obtener_resultado()

    def get_calificacion(self, instance):
        return instance.obtener_calificacion()

    class Meta:
        model = PreguntaExamenResuelto
        fields = (
            'id', 'enunciado', 'respuestas', 'respuestas_seleccionadas', 'resultado', 'calificacion',
        )



class ResponderExamenSerializer(serializers.ModelSerializer):
    preguntas = PreguntaExamenResueltoSerializer(many=True, write_only=True)

    class Meta:
        model = ExamenResuelto
        fields = (
            'id', 'preguntas',
        )

    def update(self, instance, validated_data):
        data_preguntas = validated_data.pop('preguntas')
        for data_pregunta in data_preguntas:
            respuestas_seleccionadas = data_pregunta['respuestas_seleccionadas']
            if respuestas_seleccionadas:
                pregunta = respuestas_seleccionadas[0].pregunta
                pregunta_serializer = PreguntaExamenResueltoSerializer()
                pregunta = super(PreguntaExamenResueltoSerializer, pregunta_serializer).update(pregunta, data_pregunta)

        examen = super().update(instance, validated_data)

        return examen


class ObtenerExamenResueltoSerializer(serializers.ModelSerializer):
    examen = ObtenerExamenSerializer(read_only=True)
    preguntas = ObtenerPreguntaExamenResueltoSerializer(many=True, read_only=True)
    calificacion = serializers.SerializerMethodField()

    def get_calificacion(self, instance):
        return instance.obtener_calificacion()


    class Meta:
        model = ExamenResuelto
        fields = (
            'id', 'titulo', 'estudiante', 'profesor', 'grupo', 'fecha_inicio', 'fecha_fin', 'iniciado', 'terminado', 'calificacion', 'preguntas', 'examen',
        )


class ListarExamenResueltoSerializer(serializers.ModelSerializer):
    calificacion = serializers.SerializerMethodField()
    examen = ObtenerExamenSerializer(read_only=True)

    def get_calificacion(self, instance):
        return instance.obtener_calificacion()

    class Meta:
        model = ExamenResuelto
        fields = (
            'id', 'titulo', 'estudiante', 'profesor', 'grupo', 'fecha_inicio', 'fecha_fin', 'iniciado', 'terminado', 'calificacion', 'examen',
        )


class CalificacionesExamenSerializer(serializers.ModelSerializer):
    examenes_resueltos = ListarExamenResueltoSerializer(many=True,)

    class Meta:
        model = Examen
        fields = (
            'id', 'titulo', 'profesor', 'grupo', 'fecha_inicio', 'fecha_fin', 'iniciado', 'terminado', 'examenes_resueltos'
        )