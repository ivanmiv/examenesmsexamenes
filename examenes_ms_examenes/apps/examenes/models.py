from datetime import timedelta

from django.db import models
from django.utils import timezone
from django_microservices.models import MicroService


class Pregunta(models.Model):
    creador = models.PositiveIntegerField()
    enunciado = models.TextField()

    def __str__(self):
        return "Pregunta %s" % self.id

    def respuestas_correctas(self):
        return self.respuestas.filter(es_correcta=True)


class Respuesta(models.Model):
    pregunta = models.ForeignKey('Pregunta', on_delete=models.CASCADE, related_name='respuestas')
    respuesta = models.TextField()
    imagen = models.ImageField(blank=True, null=True)
    es_correcta = models.BooleanField(default=False, verbose_name='¿Es una respuesta correcta?')

    def __str__(self):
        return self.respuesta


class Examen(models.Model):
    titulo = models.CharField(max_length=255, verbose_name='título')
    profesor = models.PositiveIntegerField()
    grupo = models.PositiveIntegerField()
    fecha_inicio = models.DateTimeField()
    fecha_fin = models.DateTimeField()
    iniciado = models.BooleanField(default=False)
    terminado = models.BooleanField(default=False)
    preguntas_originales = models.ManyToManyField(Pregunta)

    class Meta:
        ordering = ['-fecha_inicio', '-terminado', '-iniciado', 'titulo', 'profesor']

    def __str__(self):
        return "%s (%s)" % (self.titulo, self.grupo)

    def examen_disponible(self):
        return self.fecha_inicio < timezone.now() and timezone.now() < (self.fecha_fin - timedelta(seconds=1)) and self.iniciado == True and self.terminado == False

    def generar_examenes_grupo(self):
        microservicio = MicroService.objects.get(name="api")
        try:
            respuesta = microservicio.remote_call(
                    "GET", api='api/grupos/%s' % self.grupo, headers={'MICROSERVICE-USER-PK': str(self.profesor)}
            )

            if respuesta.status_code == 200:
                grupo = respuesta.json()
            else:
                grupo = {}
        except Exception as e:
            grupo = {}

        for estudiante in grupo.get('estudiantes',[]):
            ExamenResuelto.crear_nuevo_examen_resuelto(estudiante, self)

    def actualizar_preguntas(self):
        if self.iniciado or self.terminado or self.examenes_resueltos.count() != 0:
            return False
        else:
            self.preguntas.all().delete()
            for pregunta in self.preguntas_originales.all():
                pregunta_examen = PreguntaExamen.objects.create(
                        creador=pregunta.creador,
                        enunciado=pregunta.enunciado,
                        examen=self
                )
                for respuesta in pregunta.respuestas.all():
                    RespuestaExamen.objects.create(
                            pregunta=pregunta_examen,
                            respuesta=respuesta.respuesta,
                            imagen=respuesta.imagen,
                            es_correcta=respuesta.es_correcta
                    )


class PreguntaExamen(models.Model):
    creador = models.PositiveIntegerField()
    enunciado = models.TextField()
    examen = models.ForeignKey('Examen', related_name='preguntas', on_delete=models.CASCADE)

    def respuestas_correctas(self):
        return self.respuestas.filter(es_correcta=True)


class RespuestaExamen(models.Model):
    pregunta = models.ForeignKey('PreguntaExamen', on_delete=models.CASCADE, related_name='respuestas')
    respuesta = models.TextField()
    imagen = models.ImageField(blank=True)
    es_correcta = models.BooleanField(default=False, verbose_name='¿Es una respuesta correcta?')

    def __str__(self):
        return self.respuesta


class ExamenResuelto(models.Model):
    examen = models.ForeignKey(Examen, on_delete=models.SET_NULL, null=True, related_name='examenes_resueltos')
    titulo = models.CharField(max_length=255, verbose_name='título')
    estudiante = models.PositiveIntegerField()
    profesor = models.PositiveIntegerField()
    grupo = models.PositiveIntegerField()
    fecha_inicio = models.DateTimeField(verbose_name="fecha de inicio")
    fecha_fin = models.DateTimeField(verbose_name="fecha de finalización")
    iniciado = models.BooleanField(default=False)
    terminado = models.BooleanField(default=False)

    def __str__(self):
        return str(self.examen)

    class Meta:
        unique_together = ('examen', 'estudiante')
        ordering = ['grupo', '-fecha_inicio', '-terminado', '-iniciado', 'estudiante', 'profesor']

    @staticmethod
    def crear_nuevo_examen_resuelto(estudiante, examen):
        nuevo_examen = ExamenResuelto.objects.create(
                examen=examen,
                titulo=examen.titulo,
                estudiante=estudiante,
                profesor=examen.profesor,
                grupo=examen.grupo,
                fecha_inicio=examen.fecha_inicio,
                fecha_fin=examen.fecha_fin,
                iniciado=False,
        )

        for pregunta in examen.preguntas.all():
            nueva_pregunta_resuelta = PreguntaExamenResuelto.crear_nueva_pregunta_resuelta(nuevo_examen, pregunta.creador, pregunta.enunciado)

            for respuesta in pregunta.respuestas.all():
                RespuestaExamenResuelto.crear_nueva_respuesta_examen_resuelto(nueva_pregunta_resuelta, respuesta.respuesta, respuesta.imagen, respuesta.es_correcta)

        return nuevo_examen

    def examen_disponible(self):
        return self.examen.examen_disponible()

    def obtener_calificacion(self):
        calificacion = 100
        calificacion_preguntas = 0
        preguntas = self.preguntas.all().count()
        for pregunta in self.preguntas.all():
            calificacion_preguntas += pregunta.obtener_calificacion()
        calificacion = round(calificacion * (calificacion_preguntas / preguntas), 3) if preguntas>0 else 0
        return calificacion


class PreguntaExamenResuelto(models.Model):
    creador = models.PositiveIntegerField()
    examen_resuelto = models.ForeignKey('ExamenResuelto', on_delete=models.CASCADE, related_name='preguntas')
    enunciado = models.TextField()
    respuestas_seleccionadas = models.ManyToManyField('RespuestaExamenResuelto', blank=True, related_name='preguntas_resueltas_respuesta_seleccionada')

    def __str__(self):
        return self.enunciado

    @staticmethod
    def crear_nueva_pregunta_resuelta(examen, creador, enunciado):
        return PreguntaExamenResuelto.objects.create(
                examen_resuelto=examen,
                creador=creador,
                enunciado=enunciado
        )

    def obtener_respuestas_correctas(self):
        return self.respuestas.filter(es_correcta=True)

    def obtener_resultado(self):
        respuestas_correctas = self.obtener_respuestas_correctas()
        respuestas_seleccionadas = self.respuestas_seleccionadas.all()

        if respuestas_correctas.count() == respuestas_seleccionadas.count() and respuestas_correctas.difference(respuestas_seleccionadas).count() == 0:
            return 'respuesta_correcta'

        for respuesta in respuestas_seleccionadas:
            if respuesta in respuestas_correctas:
                return 'respuesta_parcialmente_correcta'

        if respuestas_seleccionadas.count() > 0:
            return 'respuesta_incorrecta'

        return 'respuesta_no_seleccionada'

    def obtener_calificacion(self):
        respuestas_correctas = self.obtener_respuestas_correctas()
        respuestas_seleccionadas = self.respuestas_seleccionadas.all()
        numero_respuestas_seleccionadas = self.respuestas_seleccionadas.all().count()

        calificacion_pregunta = 0
        calificacion_respuesta = 0 if numero_respuestas_seleccionadas == 0 else round((1 / len(respuestas_seleccionadas)), 3)

        for respuesta in respuestas_seleccionadas:
            if respuesta in respuestas_correctas:
                calificacion_pregunta += calificacion_respuesta

        return calificacion_pregunta if calificacion_pregunta <= 1 else 1


class RespuestaExamenResuelto(models.Model):
    pregunta = models.ForeignKey('PreguntaExamenResuelto', on_delete=models.CASCADE, related_name='respuestas')
    respuesta = models.TextField()
    imagen = models.ImageField(blank=True)
    es_correcta = models.BooleanField(verbose_name='¿Es una respuesta correcta?')

    def __str__(self):
        return self.respuesta

    @staticmethod
    def crear_nueva_respuesta_examen_resuelto(pregunta, respuesta, imagen, es_correcta):
        return RespuestaExamenResuelto.objects.create(
                pregunta=pregunta,
                respuesta=respuesta,
                imagen=imagen,
                es_correcta=es_correcta
        )
