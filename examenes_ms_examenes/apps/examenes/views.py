from datetime import timedelta

from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django_microservices.models import MicroService
from rest_framework import generics, status
from rest_framework.exceptions import PermissionDenied

from apps.examenes.models import Pregunta, Examen, ExamenResuelto
from apps.examenes.serializers import PreguntaSerializer, ObtenerExamenSerializer, ModificarPreguntaSerializer, ExamenSerializer, ResponderExamenSerializer, ObtenerExamenResueltoSerializer, ListarExamenResueltoSerializer, CalificacionesExamenSerializer


class ListarCrearPreguntaView(generics.ListCreateAPIView):
    queryset = Pregunta.objects.all()
    serializer_class = PreguntaSerializer

    def get_queryset(self):
        queryset = super().get_queryset()

        try:
            profesor = int(self.request.META.get('HTTP_MICROSERVICE_USER_PK'))
        except (ValueError, TypeError):
            profesor = None

        if profesor is None:
            raise PermissionDenied({})

        queryset = queryset.filter(creador=int(profesor))

        return queryset


class ObtenerModificarPreguntaView(generics.RetrieveUpdateAPIView):
    queryset = Pregunta.objects.all()
    serializer_class = ModificarPreguntaSerializer

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            profesor = int(self.request.META.get('HTTP_MICROSERVICE_USER_PK'))
        except (ValueError, TypeError):
            profesor = None
        if profesor != instance.creador:
            raise PermissionDenied({})
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            profesor = int(self.request.META.get('HTTP_MICROSERVICE_USER_PK'))
        except (ValueError, TypeError):
            profesor = None
        if profesor != instance.creador:
            raise PermissionDenied({})
        return self.update(request, *args, **kwargs)


class ListarCrearExamenView(generics.ListCreateAPIView):
    queryset = Examen.objects.all()
    serializer_class = ExamenSerializer

    def get_serializer_class(self):
        if self.request.method == "GET":
            return ObtenerExamenSerializer
        else:
            return self.serializer_class

    def get_queryset(self):
        queryset = super().get_queryset()

        try:
            usuario = int(self.request.META.get('HTTP_MICROSERVICE_USER_PK'))
        except (ValueError, TypeError):
            usuario = None

        if usuario is None:
            raise PermissionDenied({})

        microservicio = MicroService.objects.get(name="api")
        try:
            respuesta = microservicio.remote_call(
                    "GET", api='api/usuarios/%s' % usuario, headers={'MICROSERVICE-USER-PK': str(usuario)}
            )

            if respuesta.status_code == 200:
                usuario = respuesta.json()
            else:
                usuario = {}
        except Exception as e:
            usuario = {}
        if usuario.get('rol') == 'Profesor':
            queryset = queryset.filter(profesor=int(usuario.get('id', 0)))
        elif usuario.get('rol') == 'Estudiante':
            try:
                respuesta = microservicio.remote_call(
                        "GET", api='api/grupos/', headers={'MICROSERVICE-USER-PK': str(usuario.get('id'))}
                )

                if respuesta.status_code == 200:
                    respuesta = respuesta.json()
                    grupos = []
                    for grupo in respuesta:
                        if grupo.get('id'):
                            grupos.append(grupo['id'])
                else:
                    grupos = []
            except Exception as e:
                grupos = []

            queryset = queryset.filter(grupo__in=grupos, terminado=False, fecha_inicio__lte=timezone.now(), fecha_fin__gte=timezone.now() - timedelta(minutes=1))

            for examen in queryset:
                try:
                    examen_resuelto = examen.examenes_resueltos.get(estudiante=usuario.get('id'))
                    if examen_resuelto.terminado:
                        queryset = queryset.exclude(pk=examen_resuelto.examen.pk)
                except ExamenResuelto.DoesNotExist:
                    pass

        else:
            raise PermissionDenied({})
        return queryset


class ObtenerModificarExamenView(generics.RetrieveUpdateAPIView):
    queryset = Examen.objects.all()
    serializer_class = ExamenSerializer

    def get_serializer_class(self):
        if self.request.method == "GET":
            return ObtenerExamenSerializer
        else:
            return self.serializer_class

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            profesor = int(self.request.META.get('HTTP_MICROSERVICE_USER_PK'))
        except (ValueError, TypeError):
            profesor = None

        if profesor != instance.profesor:
            raise PermissionDenied({})
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            profesor = int(self.request.META.get('HTTP_MICROSERVICE_USER_PK'))
        except (ValueError, TypeError):
            profesor = None

        if profesor != instance.profesor:
            raise PermissionDenied({})
        return self.update(request, *args, **kwargs)


def iniciar_examen(request, pk):
    examen = get_object_or_404(Examen, profesor=request.META.get('HTTP_MICROSERVICE_USER_PK'), pk=pk, iniciado=False, terminado=False)
    examen.iniciado = True
    examen.actualizar_preguntas()
    examen.generar_examenes_grupo()
    examen.save()
    return JsonResponse({}, status=status.HTTP_200_OK)


def terminar_examen(request, pk):
    examen = get_object_or_404(Examen, profesor=request.META.get('HTTP_MICROSERVICE_USER_PK'), pk=pk, iniciado=True, terminado=False)
    examen.terminado = True
    examen.save()
    return JsonResponse({}, status=status.HTTP_200_OK)


class ResponderExamenView(generics.RetrieveUpdateAPIView):
    queryset = ExamenResuelto.objects.all()
    serializer_class = ResponderExamenSerializer

    def get_serializer_class(self):
        if self.request.method == "GET":
            lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
            if lookup_url_kwarg in self.kwargs:
                return ObtenerExamenResueltoSerializer
            else:
                return ListarExamenResueltoSerializer
        else:
            return self.serializer_class

    def get_object(self):
        microservicio = MicroService.objects.get(name="api")
        queryset = self.filter_queryset(self.get_queryset())

        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
                'Expected view %s to be called with a URL keyword argument '
                'named "%s". Fix your URL conf, or set the `.lookup_field` '
                'attribute on the view correctly.' %
                (self.__class__.__name__, lookup_url_kwarg)
        )
        try:
            estudiante = int(self.request.META.get('HTTP_MICROSERVICE_USER_PK'))
        except (ValueError, TypeError):
            estudiante = None

        if estudiante is None:
            raise PermissionDenied({})
        try:
            respuesta = microservicio.remote_call(
                    "GET", api='api/grupos/', headers={'MICROSERVICE-USER-PK': str(estudiante)}
            )

            if respuesta.status_code == 200:
                respuesta = respuesta.json()
                grupos = []
                for grupo in respuesta:
                    if grupo.get('id'):
                        grupos.append(grupo['id'])
            else:
                grupos = []
        except Exception as e:
            grupos = []

        examen_original = get_object_or_404(Examen, id=self.kwargs[lookup_url_kwarg], grupo__in=grupos, iniciado=True, terminado=False)  # , fecha_inicio__lte=timezone.now(), fecha_fin__gte=timezone.now() - timedelta(minutes=1))

        try:
            examen = ExamenResuelto.objects.get(examen=examen_original, estudiante=estudiante)
        except ExamenResuelto.DoesNotExist:
            examen = ExamenResuelto.crear_nuevo_examen_resuelto(estudiante, examen_original)

        return examen

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            estudiante = int(self.request.META.get('HTTP_MICROSERVICE_USER_PK'))
        except (ValueError, TypeError):
            estudiante = None
        if estudiante != instance.estudiante:
            raise PermissionDenied({})

        instance.iniciado = True
        instance.save()
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            estudiante = int(self.request.META.get('HTTP_MICROSERVICE_USER_PK'))
        except (ValueError, TypeError):
            estudiante = None

        if estudiante != instance.estudiante:
            raise PermissionDenied({})

        if not request.is_ajax():
            instance.terminado = True
            instance.save()
        return self.update(request, *args, **kwargs)


class ObtenerExamenResueltoView(generics.RetrieveAPIView):
    queryset = ExamenResuelto.objects.all()
    serializer_class = ObtenerExamenResueltoSerializer

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            usuario = int(self.request.META.get('HTTP_MICROSERVICE_USER_PK'))
        except (ValueError, TypeError):
            usuario = None
        if usuario != instance.estudiante and usuario != instance.profesor:
            raise PermissionDenied({})

        return self.retrieve(request, *args, **kwargs)


class ListarExamenResueltoView(generics.ListAPIView):
    queryset = ExamenResuelto.objects.all()
    serializer_class = ListarExamenResueltoSerializer

    def get_queryset(self):
        queryset = super().get_queryset()

        try:
            estudiante = int(self.request.META.get('HTTP_MICROSERVICE_USER_PK'))
        except (ValueError, TypeError):
            estudiante = None

        if estudiante is None:
            raise PermissionDenied({})

        queryset = queryset.filter(estudiante=estudiante)

        return queryset


class ObtenerCalificacionesExamenView(generics.RetrieveAPIView):
    queryset = Examen.objects.all()
    serializer_class = CalificacionesExamenSerializer

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            usuario = int(self.request.META.get('HTTP_MICROSERVICE_USER_PK'))
        except (ValueError, TypeError):
            usuario = None
        if usuario != instance.profesor:
            raise PermissionDenied({})

        return self.retrieve(request, *args, **kwargs)