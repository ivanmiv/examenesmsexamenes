from django.urls import path
from .views import *

urlpatterns = [
    path('preguntas', ListarCrearPreguntaView.as_view()),
    path('preguntas/<int:pk>/', ObtenerModificarPreguntaView.as_view(), name='pregunta-detail'),
    path('', ListarCrearExamenView.as_view()),
    path('<int:pk>/', ObtenerModificarExamenView.as_view(), name='examen-detail'),
    path('iniciar-examen/<int:pk>', iniciar_examen),
    path('terminar-examen/<int:pk>', terminar_examen),
    path('responder-examen/<int:pk>', ResponderExamenView.as_view()),
    path('examenes-estudiante', ListarExamenResueltoView.as_view()),
    path('calificacion-examen/<int:pk>/', ObtenerExamenResueltoView.as_view()),
    path('calificaciones-examen/<int:pk>/', ObtenerCalificacionesExamenView.as_view()),

]
